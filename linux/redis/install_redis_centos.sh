#!/bin/bash

redis_version="5.0.7"

install_path="/opt/redis"

cd $HOME

wget -c -t 0 -T 12000 http://download.redis.io/releases/redis-${redis_version}.tar.gz

tar zxvf redis-${redis_version}.tar.gz

cd redis-${redis_version}

make

make PREFIX=${install_path} install
