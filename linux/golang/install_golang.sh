#!/bin/bash

go_version="1.13.8.linux-amd64"
install_path="/opt/go"
# download golang on studygolang mirror
wget -c -t 0 -T 1200 https://studygolang.com/dl/golang/go${go_version}.tar.gz

tar zxvf go${go_version}.tar.gz

mv go $install_path

# set go mod
GO111MODULE=on
# set go mod proxy
GOPROXY=https://goproxy.cn,direct
# set environment
echo "export GOROOT=${install_path}" >>~/.bash_profile
echo "export GOPATH=${HOME}/gopath" >>~/.bash_profile
echo "export PATH=\${GOROOT}/bin:\${GOPATH}/bin:\$PATH" >>~/.bash_profile
# test the env setting
source ~/.bash_profile

go version

echo "You need run \" source ~/.bash_profile \" again !"