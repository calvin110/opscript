#!/bin/bash
# author : Jalright
# date : 2020-02-18

yum -y install xz tar gcc make tk-devel wget sqlite-devel zlib-devel readline-devel openssl-devel curl-devel tk-devel gdbm-devel xz-devel bzip2-devel

cd /root/

wget -c https://www.python.org/ftp/python/3.6.10/Python-3.6.10.tar.xz

xz -d Python-3.6.10.tar.xz

tar xvf Python-3.6.10.tar

cd Python-3.6.10

./configure --prefix=/opt/python36

make

make install

echo 'export PATH=/opt/python36/bin:$PATH' >>/etc/profile

source /etc/profile

python3 -V
