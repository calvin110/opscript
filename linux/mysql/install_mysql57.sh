#!/bin/bash
# install dependent package
yum install -y wget tar

wget -c t 0 -T 1200 https://mirrors.huaweicloud.com/mysql/Downloads/MySQL-5.7/mysql-5.7.29-linux-glibc2.12-x86_64.tar.gz

tar zxvf mysql-5.7.29-linux-glibc2.12-x86_64.tar.gz

mv mysql-5.7.29-linux-glibc2.12-x86_64 /usr/local/

cd /usr/local

ln -s mysql-5.7.29-linux-glibc2.12-x86_64 mysql

mv /etc/my.cnf   /etc/my.cnf_$(date +%s)

cp mysql/support-files/my-default.cnf /etc/my.cnf

# create work user
useradd -s /sbin/nologin -M mysql

# create datadir
mkdir -p /usr/local/mysql/data/

# modify file and direcotry perm
chown mysql.mysql /usr/local/mysql -R

chown mysql.mysql /usr/local/mysql-5.7.29-linux-glibc2.12-x86_64 -R

cd /usr/local/mysql

cp /usr/local/mysql/support-files/mysql.server /etc/init.d/mysql

# init mysql
bin/mysqld --initialize --basedir=/usr/local/mysql --datadir=/usr/local/mysql/data --user=mysql

echo "MySQL password is previous line !!!"

chmod +x  /etc/init.d/mysql

/etc/init.d/mysql start